namespace java com.ruoyi.api.log

include "common.thrift"


// 日志类型
struct LogType{
    1:string type;              // 日志标识
    2:string name;              // 日志名称
    3:map<string,LogParam> params;    // 日志参数
}


// 事件参数
struct LogParam{
    1:string key;               // 参数标识
    2:string name;              // 参数名称
    3:string type;              // 参数类型
    4:map<i32,string> values;   // 取值枚举
}

// 参数枚举
struct ParamValue{
    1:string value;             // 参数取值
    2:string name;              // 取值名称
}


// 查询结果
struct QueryResultDTO{
    1:i32 total;                    // ID
    2:list<LogData> logs;           // 参数表示
}

struct QueryConditionDTO{
    map<string,string> condition;   // 查询条件
}

// 日志数据
struct LogData{
    1:string id;                // 日志ID
    2:string type;              // 日志类型
    3:string content;           // 日志内容
    4:i64 datetime;             // 日志时间
}

/** 日志 */
service LogService {
    /** 取得所有日志类型 */
    list<LogType> getTypes();

    // 查询列表
    QueryResultDTO queryList(1:QueryConditionDTO condition,2:common.PageDTO page)
}