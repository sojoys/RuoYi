namespace java com.ruoyi.api.info


// 信息
struct InfoDTO{
    1:i32 todayAdd;                          // 今天新增
    2:i32 onlineCount;                       // 当前在线
    3:i32 totalCount;                        // 总注册
    4:i32 todayLogin;                        // 今日登陆数
}

/** 参数 */
service InfoService {
   // 查询一个
   InfoDTO getInfo();
}