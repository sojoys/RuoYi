namespace java com.ruoyi.api.talbe

include "common.thrift"


// 配置表
struct TableDTO{
    1:i64 id;                  // ID
    2:string name;             // 名称
}

/** 配置表 */
service TableService {
    // 取得道具配置
    list<TableDTO> getPropsTable()
}