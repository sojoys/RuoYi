namespace java com.ruoyi.api.common

// 分页信息
struct PageDTO{
    1:i32 pageSize;         // 分页数量
    2:i32 pageNum;          // 页数
    3:string orderByColumn; // 排序字段
    4:string isAsc;         // 排序方式
}

// 构建信息
struct BuilderDTO{
    1:string createBy;      // 创建人
    2:i64 createTime;       // 创建时间
    3:string updateBy;      // 更新人
    4:i64 updateTime;       // 更新时间
}