namespace java com.ruoyi.api.role

include "common.thrift"
include "i18n.thrift"

// 角色
struct RoleDTO{
    1:string id;                            // ID
    2:i32 number;                           // 玩家编号
    3:string name;                          // 昵称
    4:i32 lv;                               // 等级
    5:bool online;                          // 是否在线
    6:i64 lastLoginDate;                    // 最后登录时间
}

// 查询条件
struct QueryConditionDTO{
    1:string number;                        // 玩家编号
    2:string name;                          // 玩家昵称
    3:bool online;                          // 是否在线
}

// 查询结果
struct QueryResultDTO{
    1:i32 total;                    // ID
    2:list<RoleDTO> roles;       // 参数表示
}

/** 角色 */
service RoleService {
    // 查询列表
    QueryResultDTO queryList(1:QueryConditionDTO condition,2:common.PageDTO page)

    // 查询一个
    string queryOne(1:string id);

    // 取得在线人数
    i32 queryOnlineCount();

    // 重置密码
    bool resetPassword(1:string roleID,2:string password);

    // 禁言
    bool banChat(1:string roleID,2:i64 time)

    // 封号
    bool banLogin(1:string roleID,2:i64 time)

    // 踢下线
    bool kickOut(1:string roleID,2:i64 time)

    // 详细
    string detailed(1:string roleID)

    // 执行命令
    bool execCommand(1:i64 roleId,2:string cmd,3:string param)
}