namespace java com.ruoyi.api.config

include "common.thrift"


// 参数
struct ConfigDTO{
    1:i64 id;                       // ID
    2:string key;                   // 参数键
    3:string val;                   // 参数值
    4:common.BuilderDTO builder;    // 构建信息
}

// 查询条件
struct QueryConditionDTO{
    1:string id;
    2:string key;
}

// 查询结果
struct QueryResultDTO{
    1:i32 total;                    // ID
    2:list<ConfigDTO> config;       // 参数表示
}

/** 参数 */
service ConfigService {
    // 查询列表
    QueryResultDTO queryList(1:QueryConditionDTO condition,2:common.PageDTO page)

    // 查询一个
    ConfigDTO queryOne(1:QueryConditionDTO condition);

    // 保存
    i64 save(1:ConfigDTO config)

    // 删除
    i64 remove(1:list<i64> ids)
}