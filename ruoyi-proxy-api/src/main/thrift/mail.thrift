namespace java com.ruoyi.api.mail

include "common.thrift"
include "i18n.thrift"

// 邮件
struct MailDTO{
    1:i64 id;                           // ID
    2:i8 mailType;                      // 邮件类型
    3:i8 sendType;                      // 发送类型
    4:i64 sendTime;                     // 发送时间
    5:string title;                     // 标题文字
    6:string content;                   // 内容文字
    7:i32 status;                       // 状态
    8:string recipient;                 // 收件人
    9:string annex;                     // 附件
}

// 查询条件
struct QueryConditionDTO{
    1:string id;
    2:string mailType;
    3:string sendType;
    4:string title;
    5:string status;
}

// 查询结果
struct QueryResultDTO{
    1:i32 total;                    // ID
    2:list<MailDTO> mails;       // 参数表示
}

/** 邮件 */
service MailService {
    // 查询列表
    QueryResultDTO queryList(1:QueryConditionDTO condition,2:common.PageDTO page)

    // 查询一个
    MailDTO queryOne(1:QueryConditionDTO condition);

    // 保存
    i64 save(1:MailDTO mail)

    // 删除
    i64 remove(1:list<i64> ids)

    // 发送
    i64 sendMail(1:list<i64> ids)
}