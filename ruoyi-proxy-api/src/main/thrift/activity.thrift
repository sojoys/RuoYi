namespace java com.ruoyi.api.activity

include "common.thrift"


// 活动
struct ActivityDataDTO{
    1:i32 id;                               // ID
    2:string activityName;                  // 名称
    3:i32 activityType;                     // 类型
    4:i32 activitySwitch;                  // 活动开关
    5:i32 timeType;                         // 开启时间类型
    6:i32 timeParam;                        // 开始时间
    7:string effectTime;                    // 持续时间
    8:i32 orderNum;                         // 排序编号
    9:string description;                   // 描述
    10:i32 uiType;                          // ui类型
}

// 查询条件
struct ActivityDataQueryConditionDTO{
    1:string id;
    2:string name;
    3:string type;
}

// 查询结果
struct ActivityDataQueryResultDTO{
    1:i32 total;                        // ID
    2:list<ActivityDataDTO> activitys;      // 活动集
}

/** 国际化 */
service ActivityDataService {
    // 查询列表
    ActivityDataQueryResultDTO queryList(1:ActivityDataQueryConditionDTO condition,2:common.PageDTO page)

    // 根据活动ID查询
    ActivityDataDTO queryById(1:i32 id)

    // 查询列表
    list<ActivityDataDTO> queryAll()

    // 查询一个
    ActivityDataDTO queryOne(1:ActivityDataQueryConditionDTO condition);

    // 保存
    i32 save(1:ActivityDataDTO activity)

    // 删除
    i32 remove(1:list<i32> ids)

    // 上传活动
    i32 upload(1:list<i32> ids)
}


// 活动条目
struct ActivityItemDTO{
    1:i32 id;                           // ID
    2:i32 activityId;                   // 活动ID
    3:string itemName;                      // 名称
    4:string itemIcon;                      // 图标
    5:string background;                // 背景图
    6:i32 targetType;                   // 目标类型
    7:string targetParam;               // 目标参数
    8:i32 targetSchedule;               // 目标进度
    9:i32 actionType;                   // 行为类型
    10:string actionParam;              // 行为参数
    11:i32 itemSwitch;                 // 激活
    12:i32 orderNum;                    // 排序编号
    13:string remark;                   // 备注
}

// 查询条件
struct ActivityItemQueryConditionDTO{
    1:string id;
    2:string activityId;
}

// 查询结果
struct ActivityItemQueryResultDTO{
    1:i32 total;                    // ID
    2:list<ActivityItemDTO> datas;       // 参数表示
}

/** 活动条目 */
service ActivityItemService {
    // 查询列表
    ActivityItemQueryResultDTO queryList(1:ActivityItemQueryConditionDTO condition,2:common.PageDTO page)

    // 查询一个
    ActivityItemDTO queryOne(1:ActivityItemQueryConditionDTO condition);

    // 保存
    i32 save(1:ActivityItemDTO ActivityItem)

    // 删除
    i32 remove(1:list<i32> ids)
}