namespace java com.ruoyi.api.notice

include "common.thrift"


// 公告
struct NoticeDTO{
    1:i64 id;               // ID
    2:i32 label;            // 标签
    3:i32 type;             // 类型
    4:string title;         // 标题
    5:string content;       // 内容
}

// 查询条件
struct QueryConditionDTO{
    1:string id;
    2:string label;            // 标签
    3:string type;             // 类型
    4:string title;         // 标题
}

// 查询结果
struct QueryResultDTO{
    1:i32 total;                    // ID
    2:list<NoticeDTO> mails;       // 参数表示
}

/** 公告 */
service NoticeService {
    // 查询列表
    QueryResultDTO queryList(1:QueryConditionDTO condition,2:common.PageDTO page)

    // 查询一个
    NoticeDTO queryOne(1:QueryConditionDTO condition);

    // 保存
    i64 save(1:NoticeDTO notice)

    // 删除
    i64 remove(1:list<i64> ids)
}