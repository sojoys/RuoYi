namespace java com.ruoyi.api.keep

include "common.thrift"
include "i18n.thrift"

// 封禁
struct KeepDTO{
    1:i64 keep_date;
    2:i32 new_count;
    3:double day_1;
    4:double day_3;
    5:double day_7;
    6:double day_30;
}

// 查询条件
struct QueryConditionDTO{
    1:string startDate;
    2:string endDate;
}

// 查询结果
struct QueryResultDTO{
    1:i32 total;                // ID
    2:list<KeepDTO> roles;       // 参数表示
}

/** 参数 */
service KeepService {
    // 查询列表
    QueryResultDTO queryList(1:QueryConditionDTO condition,2:common.PageDTO page)
}