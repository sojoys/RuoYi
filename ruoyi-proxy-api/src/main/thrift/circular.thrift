namespace java com.ruoyi.api.circular

include "common.thrift"
include "i18n.thrift"

// 通知
struct CircularDTO{
    1:i32 id;                           // ID
    2:i32 tag;                          // 玩家编号
    3:string content;                   // 通知内容
    4:i32 interval;                     // 时间间隔
    5:i32 pushCount;                    // 已发送次数
    6:i64 startDate;                    // 开始时间
    7:i64 endDate;                      // 结束时间
}

// 查询条件
struct QueryConditionDTO{
    1:string id;                           // ID
}

// 查询结果
struct QueryResultDTO{
    1:i32 total;                        // ID
    2:list<CircularDTO> datas;          // 参数表示
}

/** 参数 */
service CircularService {
    // 查询列表
    QueryResultDTO queryList(1:QueryConditionDTO condition,2:common.PageDTO page)

    // 查询一个
    CircularDTO queryOne(1:QueryConditionDTO condition);

    // 保存
    i64 save(1:CircularDTO ban)

    // 删除
    i64 remove(1:list<i64> ids)
}