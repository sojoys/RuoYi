namespace java com.ruoyi.api.ban

include "common.thrift"
include "i18n.thrift"

// 封禁
struct BanDTO{
    1:i32 id;                           // ID
    2:i32 number;                       // 玩家编号
    3:i32 type;                         // 封禁类型
    4:i64 invalidDate;                  // 截止时间
    5:string remark;                    // 备注
    6:i32 offline;                      // 强制离线
}

// 查询条件
struct QueryConditionDTO{
    1:string id;
}

// 查询结果
struct QueryResultDTO{
    1:i32 total;                // ID
    2:list<BanDTO> roles;       // 参数表示
}

/** 参数 */
service BanService {
    // 查询列表
    QueryResultDTO queryList(1:QueryConditionDTO condition,2:common.PageDTO page)

    // 查询一个
    BanDTO queryOne(1:QueryConditionDTO condition);

    // 保存
    i64 save(1:BanDTO ban)

    // 删除
    i64 remove(1:list<i64> ids)
}