namespace java com.ruoyi.api.i18n

include "common.thrift"


// 国际化文字类型
struct I18nTypeDTO{
    1:i64 id;               // ID
    2:string key;           // 词组
    3:string remark;        // 备注
}

// 查询条件
struct I18nTypeQueryConditionDTO{
    1:string id;
    2:string key;
}

// 查询结果
struct I18nTypeQueryResultDTO{
    1:i32 total;                    // ID
    2:list<I18nTypeDTO> types;      // 参数表示
}

/** 国际化 */
service I18nTypeService {
    // 查询列表
    I18nTypeQueryResultDTO queryList(1:I18nTypeQueryConditionDTO condition,2:common.PageDTO page)

    // 查询一个
    I18nTypeDTO queryOne(1:I18nTypeQueryConditionDTO condition);

    // 保存
    i64 save(1:I18nTypeDTO i18nType)

    // 删除
    i64 remove(1:list<i64> ids)
}


// 国际化文字数据
struct I18nDataDTO{
    1:i64 id;               // ID
    2:string key;           // 词组
    3:i64 language;         // 语言
    4:string val;           // 内容
}

// 查询条件
struct I18nDataQueryConditionDTO{
    1:string id;
    2:string key;           // 词组
    3:string language;      // 语言
}

// 查询结果
struct I18nDataQueryResultDTO{
    1:i32 total;                    // ID
    2:list<I18nDataDTO> datas;       // 参数表示
}

/** 国际化 */
service I18nDataService {
    // 查询列表
    I18nDataQueryResultDTO queryList(1:I18nDataQueryConditionDTO condition,2:common.PageDTO page)

    // 查询一个
    I18nDataDTO queryOne(1:I18nDataQueryConditionDTO condition);

    // 保存
    i64 save(1:I18nDataDTO i18nData)

    // 删除
    i64 remove(1:list<i64> ids)
}