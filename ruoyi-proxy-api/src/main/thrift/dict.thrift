namespace java com.ruoyi.api.dict


// 字典
struct DictDTO{
    1:string dictLabel;              // 名称
    2:i32 dictValue;                // 标签
    3:bool isDefault  = false;  // 是否默认
    4:string listClass = "";
    5:string cssClass = "";
}

// 字典类型
enum DictTypeEuem{
    MAIL_TYPE,              // 邮件类型
    MAIL_SEND_TYPE,         // 邮件发送类型
    MAIL_STATUS,            // 邮件状态

    NOTICE_TYPE,            // 公告类型
    NOTICE_label,           // 公告标签


    BAN_TYPE,               // 封禁类型
    ACTIVITY_DATE_TYPE,     // 活动时间类型
    ACTIVITY_TYPE,          // 活动类型
    ACTIVITY_SWITCH,        // 活动开关

    CIRCULAR_TYPE,          // 通知类型
}


service DictService{
    // 取得字典集
    list<DictDTO> queryList(DictTypeEuem type)

    // 查询具体字
    DictDTO queryOne(1:DictTypeEuem type,2:i32 lable);
}