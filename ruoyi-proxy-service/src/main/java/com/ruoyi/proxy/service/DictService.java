package com.ruoyi.proxy.service;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.api.dict.DictDTO;
import com.ruoyi.api.dict.DictTypeEuem;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("proxyDict")
public class DictService
{
    @Autowired
    private ThriftUtil thriftUtil;

    /**
     * 根据字典类型查询字典数据信息
     * 
     * @param dictType 字典类型
     * @return 参数键值
     */
    public List<DictDTO> getType(String dictType) throws TException {
        DictTypeEuem dictTypeEuem = DictTypeEuem.findByValue(Integer.valueOf(dictType));
        return thriftUtil.getDictService().queryList(dictTypeEuem);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    public String getLabel(String dictType, String dictValue) throws TException {
        DictTypeEuem dictTypeEuem = DictTypeEuem.findByValue(Integer.valueOf(dictType));
        List<DictDTO> dicts = thriftUtil.getDictService().queryList(dictTypeEuem);
        return CollUtil.findOne(dicts,d -> d.dictValue == Integer.valueOf(dictValue)).dictLabel;
    }
}