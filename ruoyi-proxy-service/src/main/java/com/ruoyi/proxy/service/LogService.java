package com.ruoyi.proxy.service;

import com.ruoyi.api.log.LogType;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("proxyLog")
public class LogService
{
    @Autowired
    private ThriftUtil thriftUtil;

    public List<LogType> getTypes() throws TException {
        List<LogType> types = thriftUtil.getLogService().getTypes();
        return types;
    }
}