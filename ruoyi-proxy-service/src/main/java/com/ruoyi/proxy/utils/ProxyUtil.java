package com.ruoyi.proxy.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Filter;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProxyUtil {
    public static List<Long> splitToLong(CharSequence str, CharSequence separator) {
        long[] array = StrUtil.splitToLong(str, separator);
        List<Long> list = new ArrayList<>(array.length);
        for (long l : array) {
            list.add(l);
        }
        return list;
    }

    public static List<Integer> splitToInt(CharSequence str, CharSequence separator) {
        int[] array = StrUtil.splitToInt(str, separator);
        List<Integer> list = new ArrayList<>(array.length);
        for (int l : array) {
            list.add(l);
        }
        return list;
    }

    public static <T> T toBean(Map<String, String> source, Class<T> clazz) {
        Map<String, String> map = MapUtil.filter(source, new Filter<Map.Entry<String, String>>() {
            @Override
            public boolean accept(Map.Entry<String, String> entry) {
                return StrUtil.isNotEmpty(entry.getValue());
            }
        });
        return BeanUtil.toBean(map, clazz);
    }
}
