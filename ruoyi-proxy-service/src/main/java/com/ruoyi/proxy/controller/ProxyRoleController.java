package com.ruoyi.proxy.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.json.JSONUtil;
import com.ruoyi.api.common.PageDTO;
import com.ruoyi.api.role.QueryConditionDTO;
import com.ruoyi.api.role.QueryResultDTO;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import com.ruoyi.proxy.utils.ProxyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 角色管理Controller
 *
 * @author SandKing
 * @date 2020-11-26
 */
@Controller
@RequestMapping("/proxy/role")
public class ProxyRoleController extends BaseController {
    private String prefix = "proxy/role";

    @Autowired
    private ThriftUtil thriftUtil;

    @RequiresPermissions("proxy:role:view")
    @GetMapping()
    public String role() {
        return prefix + "/role";
    }

    /**
     * 查询角色管理列表
     */
    @RequiresPermissions("proxy:role:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestParam Map<String, String> param) throws TException {
        QueryConditionDTO conditionDTO = ProxyUtil.toBean(param, QueryConditionDTO.class);
        PageDTO pageDTO = BeanUtil.toBean(param, PageDTO.class);
        QueryResultDTO resultDTO = thriftUtil.getRoleService().queryList(conditionDTO, pageDTO);
        return new TableDataInfo(resultDTO.roles, resultDTO.total);
    }


    @RequiresPermissions("proxy:role:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) throws TException {
        String json = thriftUtil.getRoleService().detailed(id);
        mmap.put("role", JSONUtil.parseObj(json));
        return prefix + "/detail";
    }


//    /**
//     * 导出角色管理列表
//     */
//    @RequiresPermissions("proxy:role:export")
//    @Log(title = "角色管理", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ResponseBody
//    public AjaxResult export(TRole tRole)
//    {
//        List<TRole> list = tRoleService.selectTRoleList(tRole);
//        ExcelUtil<TRole> util = new ExcelUtil<TRole>(TRole.class);
//        return util.exportExcel(list, "role");
//    }
//


    @GetMapping("/command/{userId}")
    public String command(@PathVariable("userId") Long userId, ModelMap mmap) {
        mmap.put("userId", userId);
        return prefix + "/command";
    }

    //    @RequiresPermissions("system:user:resetPwd")
//    @Log(title = "重置密码", businessType = BusinessType.UPDATE)
    @PostMapping("/command")
    @ResponseBody
    public AjaxResult commandExec(@RequestParam Map<String, String> param) {
        long user_number = MapUtil.getLong(param, "userId");
        String cmd = MapUtil.getStr(param, "cmd");
        String params = MapUtil.getStr(param, "param");

        try {
            thriftUtil.getRoleService().execCommand(user_number, cmd, params);
            return success();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return error();
    }
//
//    /**
//     * 新增保存角色管理
//     */
//    @RequiresPermissions("proxy:role:add")
//    @Log(title = "角色管理", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    @ResponseBody
//    public AjaxResult addSave(TRole tRole)
//    {
//        return toAjax(tRoleService.insertTRole(tRole));
//    }
//
//    /**
//     * 修改角色管理
//     */
//    @GetMapping("/edit/{id}")
//    public String edit(@PathVariable("id") Long id, ModelMap mmap)
//    {
//        TRole tRole = tRoleService.selectTRoleById(id);
//        mmap.put("tRole", tRole);
//        return prefix + "/edit";
//    }
//
//    /**
//     * 修改保存角色管理
//     */
//    @RequiresPermissions("proxy:role:edit")
//    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
//    @PostMapping("/edit")
//    @ResponseBody
//    public AjaxResult editSave(TRole tRole)
//    {
//        return toAjax(tRoleService.updateTRole(tRole));
//    }
//
//    /**
//     * 删除角色管理
//     */
//    @RequiresPermissions("proxy:role:remove")
//    @Log(title = "角色管理", businessType = BusinessType.DELETE)
//    @PostMapping( "/remove")
//    @ResponseBody
//    public AjaxResult remove(String ids)
//    {
//        return toAjax(tRoleService.deleteTRoleByIds(ids));
//    }
}