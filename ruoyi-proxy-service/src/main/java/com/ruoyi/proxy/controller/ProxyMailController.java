package com.ruoyi.proxy.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Editor;
import cn.hutool.core.map.MapUtil;
import com.ruoyi.api.common.PageDTO;
import com.ruoyi.api.mail.MailDTO;
import com.ruoyi.api.mail.QueryConditionDTO;
import com.ruoyi.api.mail.QueryResultDTO;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import com.ruoyi.proxy.utils.ProxyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 邮件Controller
 *
 * @author SandKing
 * @date 2020-10-29
 */
@Controller
@RequestMapping("/proxy/mail")
public class ProxyMailController extends BaseController {
    private String prefix = "proxy/mail";

    @Autowired
    private ThriftUtil thriftUtil;

    @RequiresPermissions("proxy:mail:view")
    @GetMapping()
    public String mail() {
        return prefix + "/mail";
    }

    /**
     * 查询邮件列表
     */
    @RequiresPermissions("proxy:mail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestParam Map<String, String> param) throws TException {
        QueryConditionDTO conditionDTO = ProxyUtil.toBean(param, QueryConditionDTO.class);
        PageDTO pageDTO = BeanUtil.toBean(param, PageDTO.class);
        QueryResultDTO resultDTO = thriftUtil.getMailService().queryList(conditionDTO, pageDTO);
        return new TableDataInfo(resultDTO.mails, resultDTO.total);
    }

//    /**
//     * 导出邮件列表
//     */
//    @RequiresPermissions("proxy:mail:export")
//    @Log(title = "邮件", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ResponseBody
//    public AjaxResult export(ProxyMail proxyMail)
//    {
//        List<ProxyMail> list = proxyMailService.selectProxyMailList(proxyMail);
//        ExcelUtil<ProxyMail> util = new ExcelUtil<ProxyMail>(ProxyMail.class);
//        return util.exportExcel(list, "mail");
//    }

    /**
     * 新增邮件
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存邮件
     */
    @RequiresPermissions("proxy:mail:add")
    @Log(title = "邮件", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("sendTime")){
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        MailDTO mailDTO = BeanUtil.toBean(param, MailDTO.class);
        return toAjax(thriftUtil.getMailService().save(mailDTO) > 0);
    }

    public static void main(String[] args) {

    }

    /**
     * 修改邮件
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) throws TException {
        MailDTO mailDTO = thriftUtil.getMailService().queryOne(new QueryConditionDTO().setId(id));
        mmap.put("mailDTO", mailDTO);
        return prefix + "/edit";
    }

    /**
     * 修改保存邮件
     */
    @RequiresPermissions("proxy:mail:edit")
    @Log(title = "邮件", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("sendTime")){
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        MailDTO mailDTO = BeanUtil.toBean(param, MailDTO.class);
        return toAjax(thriftUtil.getMailService().save(mailDTO) > 0);
    }

    /**
     * 删除邮件
     */
    @RequiresPermissions("proxy:mail:remove")
    @Log(title = "邮件", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) throws TException {
        List<Long> _ids = ProxyUtil.splitToLong(ids, ",");
        return toAjax(thriftUtil.getMailService().remove(_ids) > 0);
    }


    /**
     * 删除邮件
     */
    @RequiresPermissions("proxy:mail:remove")
    @Log(title = "邮件", businessType = BusinessType.DELETE)
    @PostMapping("/send")
    @ResponseBody
    public AjaxResult send(String ids) throws TException {
        List<Long> _ids = ProxyUtil.splitToLong(ids, ",");
        return toAjax(thriftUtil.getMailService().sendMail(_ids) > 0);
    }
}
