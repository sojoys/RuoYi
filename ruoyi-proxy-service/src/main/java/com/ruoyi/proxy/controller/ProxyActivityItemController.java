package com.ruoyi.proxy.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Editor;
import cn.hutool.core.map.MapUtil;
import com.ruoyi.api.activity.ActivityItemDTO;
import com.ruoyi.api.activity.ActivityItemQueryConditionDTO;
import com.ruoyi.api.activity.ActivityItemQueryResultDTO;
import com.ruoyi.api.common.PageDTO;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import com.ruoyi.proxy.utils.ProxyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 活动Controller
 *
 * @author SandKing
 * @date 2021-02-08
 */
@Controller
@RequestMapping("/proxy/activity/item")
public class ProxyActivityItemController extends BaseController {
    private String prefix = "proxy/activity/item";

    @Autowired
    private ThriftUtil thriftUtil;

    @RequiresPermissions("proxy:activity:view")
    @GetMapping()
    public String activity() {
        return prefix + "/item";
    }

    /**
     * 查询活动列表
     */
    @RequiresPermissions("proxy:activity:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestParam Map<String, String> param) throws TException {
        ActivityItemQueryConditionDTO conditionDTO = ProxyUtil.toBean(param, ActivityItemQueryConditionDTO.class);
        PageDTO pageDTO = BeanUtil.toBean(param, PageDTO.class);
        ActivityItemQueryResultDTO resultDTO = thriftUtil.getActivityItemService().queryList(conditionDTO, pageDTO);
        return new TableDataInfo(resultDTO.datas, resultDTO.total);
    }

//    /**
//     * 导出活动列表
//     */
//    @RequiresPermissions("proxy:activity:export")
//    @Log(title = "活动", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ResponseBody
//    public AjaxResult export(TActivity tActivity)
//    {
//        List<TActivity> list = tActivityService.selectTActivityList(tActivity);
//        ExcelUtil<TActivity> util = new ExcelUtil<TActivity>(TActivity.class);
//        return util.exportExcel(list, "activity");
//    }

    /**
     * 新增活动
     */
    @GetMapping("/add/{activityId}")
    public String add(@PathVariable("activityId") Integer activityId, ModelMap mmap) {
        mmap.put("activityId", activityId);
        return prefix + "/add";
    }

    /**
     * 新增保存活动
     */
    @RequiresPermissions("proxy:activity:add")
    @Log(title = "活动", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("invalidDate")) {
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        ActivityItemDTO activityDTO = BeanUtil.toBean(param, ActivityItemDTO.class);
        return toAjax(thriftUtil.getActivityItemService().save(activityDTO) > 0);
    }

    /**
     * 修改活动
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) throws TException {
        ActivityItemDTO activityDTO = thriftUtil.getActivityItemService().queryOne(new ActivityItemQueryConditionDTO().setId(id));
        mmap.put("item", activityDTO);
        return prefix + "/edit";
    }

    /**
     * 修改保存活动
     */
    @RequiresPermissions("proxy:activity:edit")
    @Log(title = "活动", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("invalidDate")) {
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        ActivityItemDTO activityDTO = BeanUtil.toBean(param, ActivityItemDTO.class);
        return toAjax(thriftUtil.getActivityItemService().save(activityDTO) > 0);
    }

    /**
     * 删除活动
     */
    @RequiresPermissions("proxy:activity:remove")
    @Log(title = "活动", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) throws TException {
        List<Integer> _ids = ProxyUtil.splitToInt(ids, ",");
        return toAjax(thriftUtil.getActivityItemService().remove(_ids) > 0);
    }
}
