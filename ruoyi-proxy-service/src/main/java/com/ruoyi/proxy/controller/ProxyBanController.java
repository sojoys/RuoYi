package com.ruoyi.proxy.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Editor;
import cn.hutool.core.map.MapUtil;
import com.ruoyi.api.ban.BanDTO;
import com.ruoyi.api.ban.QueryConditionDTO;
import com.ruoyi.api.ban.QueryResultDTO;
import com.ruoyi.api.common.PageDTO;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import com.ruoyi.proxy.utils.ProxyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 封禁管理Controller
 * 
 * @author SandKing
 * @date 2020-11-26
 */
@Controller
@RequestMapping("/proxy/ban")
public class ProxyBanController extends BaseController
{
    private String prefix = "proxy/ban";

    @Autowired
    private ThriftUtil thriftUtil;

    @RequiresPermissions("proxy:ban:view")
    @GetMapping()
    public String ban()
    {
        return prefix + "/ban";
    }

    /**
     * 查询封禁管理列表
     */
    @RequiresPermissions("proxy:ban:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestParam Map<String, String> param) throws TException {
        QueryConditionDTO conditionDTO = ProxyUtil.toBean(param, QueryConditionDTO.class);
        PageDTO pageDTO = BeanUtil.toBean(param, PageDTO.class);
        QueryResultDTO resultDTO = thriftUtil.getBanService().queryList(conditionDTO, pageDTO);
        return new TableDataInfo(resultDTO.roles, resultDTO.total);
    }



    /**
     * 新增封禁管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存封禁管理
     */
    @RequiresPermissions("proxy:ban:add")
    @Log(title = "封禁管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("invalidDate")){
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        BanDTO banDTO = BeanUtil.toBean(param, BanDTO.class);
        return toAjax(thriftUtil.getBanService().save(banDTO) > 0);
    }

    /**
     * 修改封禁管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) throws TException {
        BanDTO banDTO = thriftUtil.getBanService().queryOne(new QueryConditionDTO().setId(id));
        mmap.put("proxyBan", banDTO);
        return prefix + "/edit";
    }

    /**
     * 修改保存封禁管理
     */
    @RequiresPermissions("proxy:ban:edit")
    @Log(title = "封禁管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("invalidDate")){
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        BanDTO banDTO = BeanUtil.toBean(param, BanDTO.class);
        return toAjax(thriftUtil.getBanService().save(banDTO) > 0);
    }

    /**
     * 删除封禁管理
     */
    @RequiresPermissions("proxy:ban:remove")
    @Log(title = "封禁管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids) throws TException {
        List<Long> _ids = ProxyUtil.splitToLong(ids, ",");
        return toAjax(thriftUtil.getBanService().remove(_ids) > 0);
    }
}