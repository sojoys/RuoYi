package com.ruoyi.proxy.controller;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.api.common.PageDTO;
import com.ruoyi.api.notice.NoticeDTO;
import com.ruoyi.api.notice.QueryConditionDTO;
import com.ruoyi.api.notice.QueryResultDTO;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import com.ruoyi.proxy.utils.ProxyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 通知公告Controller
 *
 * @author SandKing
 * @date 2020-10-23
 */
@Controller
@RequestMapping("/proxy/notice")
public class ProxyNoticeController extends BaseController {
    private String prefix = "proxy/notice";

    @Autowired
    private ThriftUtil thriftUtil;

    @RequiresPermissions("proxy:notice:view")
    @GetMapping()
    public String notice() {
        return prefix + "/notice";
    }

    /**
     * 查询通知公告列表
     */
    @RequiresPermissions("proxy:notice:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestParam Map<String, String> param) throws TException {
        QueryConditionDTO conditionDTO = ProxyUtil.toBean(param, QueryConditionDTO.class);
        PageDTO pageDTO = BeanUtil.toBean(param, PageDTO.class);
        QueryResultDTO resultDTO = thriftUtil.getNoticeService().queryList(conditionDTO, pageDTO);
        return new TableDataInfo(resultDTO.mails, resultDTO.total);
    }

//    /**
//     * 导出通知公告列表
//     */
//    @RequiresPermissions("proxy:notice:export")
//    @Log(title = "通知公告", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ResponseBody
//    public AjaxResult export(ProxyNotice proxyNotice) {
//        List<ProxyNotice> list = proxyNoticeService.selectProxyNoticeList(proxyNotice);
//        ExcelUtil<ProxyNotice> util = new ExcelUtil<ProxyNotice>(ProxyNotice.class);
//        return util.exportExcel(list, "notice");
//    }

    /**
     * 新增通知公告
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存通知公告
     */
    @RequiresPermissions("proxy:notice:add")
    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestParam Map<String, String> param) throws TException {
        NoticeDTO noticeDTO = BeanUtil.toBean(param, NoticeDTO.class);
        return toAjax(thriftUtil.getNoticeService().save(noticeDTO) > 0);
    }

    /**
     * 修改通知公告
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) throws TException {
        NoticeDTO noticeDTO = thriftUtil.getNoticeService().queryOne(new QueryConditionDTO().setId(id));
        mmap.put("noticeDTO", noticeDTO);
        return prefix + "/edit";
    }

    /**
     * 修改保存通知公告
     */
    @RequiresPermissions("proxy:notice:edit")
    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestParam Map<String, String> param) throws TException {
        NoticeDTO noticeDTO = BeanUtil.toBean(param, NoticeDTO.class);
        return toAjax(thriftUtil.getNoticeService().save(noticeDTO) > 0);
    }

    /**
     * 删除通知公告
     */
    @RequiresPermissions("proxy:notice:remove")
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) throws TException {
        List<Long> _ids = ProxyUtil.splitToLong(ids, ",");
        return toAjax(thriftUtil.getNoticeService().remove(_ids) > 0);
    }


    /**
     * 预览公告
     */
    @GetMapping("/preview/{noticeId}")
    public String preview(@PathVariable("noticeId") String noticeId, ModelMap mmap) throws IOException, TException {
//        NoticeDTO noticeDTO = thriftUtil.getNoticeService().queryOne(new QueryConditionDTO().setId(noticeId));
//        mmap.put("noticeDTO", noticeDTO);
//        return prefix + "/preview";
        return redirect("http://192.168.0.179:8090/notice/openNotice?id=" + noticeId);
    }
}
