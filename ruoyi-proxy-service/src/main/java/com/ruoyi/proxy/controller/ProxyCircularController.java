package com.ruoyi.proxy.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Editor;
import cn.hutool.core.map.MapUtil;
import com.ruoyi.api.circular.CircularDTO;
import com.ruoyi.api.circular.QueryConditionDTO;
import com.ruoyi.api.circular.QueryResultDTO;
import com.ruoyi.api.common.PageDTO;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import com.ruoyi.proxy.utils.ProxyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 通知管理Controller
 *
 * @author SandKing
 * @date 2020-11-26
 */
@Controller
@RequestMapping("/proxy/circular")
public class ProxyCircularController extends BaseController
{
    private String prefix = "proxy/circular";

    @Autowired
    private ThriftUtil thriftUtil;

    @RequiresPermissions("proxy:circular:view")
    @GetMapping()
    public String ban()
    {
        return prefix + "/circular";
    }

    /**
     * 查询通知管理列表
     */
    @RequiresPermissions("proxy:circular:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestParam Map<String, String> param) throws TException {
        QueryConditionDTO conditionDTO = ProxyUtil.toBean(param, QueryConditionDTO.class);
        PageDTO pageDTO = BeanUtil.toBean(param, PageDTO.class);
        QueryResultDTO resultDTO = thriftUtil.getCircularService().queryList(conditionDTO, pageDTO);
        return new TableDataInfo(resultDTO.datas, resultDTO.total);
    }



    /**
     * 新增通知管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存通知管理
     */
    @RequiresPermissions("proxy:circular:add")
    @Log(title = "通知管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("startDate")){
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }

                if (entry.getKey().equals("endDate")){
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        CircularDTO circularDTO = BeanUtil.toBean(param, CircularDTO.class);
        return toAjax(thriftUtil.getCircularService().save(circularDTO) > 0);
    }

    /**
     * 修改通知管理
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) throws TException {
        CircularDTO circularDTO = thriftUtil.getCircularService().queryOne(new QueryConditionDTO().setId(id));
        mmap.put("circular", circularDTO);
        return prefix + "/edit";
    }

    /**
     * 修改保存通知管理
     */
    @RequiresPermissions("proxy:circular:edit")
    @Log(title = "通知管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("startDate")){
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }

                if (entry.getKey().equals("endDate")){
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        CircularDTO circularDTO = BeanUtil.toBean(param, CircularDTO.class);
        return toAjax(thriftUtil.getCircularService().save(circularDTO) > 0);
    }

    /**
     * 删除通知管理
     */
    @RequiresPermissions("proxy:circular:remove")
    @Log(title = "通知管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids) throws TException {
        List<Long> _ids = ProxyUtil.splitToLong(ids, ",");
        return toAjax(thriftUtil.getCircularService().remove(_ids) > 0);
    }
}