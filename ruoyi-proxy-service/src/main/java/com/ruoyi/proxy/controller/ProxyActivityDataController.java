package com.ruoyi.proxy.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Editor;
import cn.hutool.core.map.MapUtil;
import com.ruoyi.api.activity.ActivityDataDTO;
import com.ruoyi.api.activity.ActivityDataQueryConditionDTO;
import com.ruoyi.api.activity.ActivityDataQueryResultDTO;
import com.ruoyi.api.common.PageDTO;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import com.ruoyi.proxy.utils.ProxyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 活动Controller
 *
 * @author SandKing
 * @date 2021-02-08
 */
@Controller
@RequestMapping("/proxy/activity/data")
public class ProxyActivityDataController extends BaseController {
    private String prefix = "proxy/activity/data";

    @Autowired
    private ThriftUtil thriftUtil;

    @RequiresPermissions("proxy:activity:view")
    @GetMapping()
    public String activity() {
        return prefix + "/data";
    }

    /**
     * 查询活动列表
     */
    @RequiresPermissions("proxy:activity:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestParam Map<String, String> param) throws TException {
        ActivityDataQueryConditionDTO conditionDTO = ProxyUtil.toBean(param, ActivityDataQueryConditionDTO.class);
        PageDTO pageDTO = BeanUtil.toBean(param, PageDTO.class);
        ActivityDataQueryResultDTO resultDTO = thriftUtil.getActivityDataService().queryList(conditionDTO, pageDTO);
        return new TableDataInfo(resultDTO.activitys, resultDTO.total);
    }

    /**
     * 新增活动
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存活动
     */
    @RequiresPermissions("proxy:activity:add")
    @Log(title = "活动", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("invalidDate")) {
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        ActivityDataDTO activityDTO = BeanUtil.toBean(param, ActivityDataDTO.class);
        return toAjax(thriftUtil.getActivityDataService().save(activityDTO) > 0);
    }

    /**
     * 修改活动
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) throws TException {
        ActivityDataDTO activityDTO = thriftUtil.getActivityDataService().queryOne(new ActivityDataQueryConditionDTO().setId(id));
        mmap.put("activity", activityDTO);
        return prefix + "/edit";
    }

    /**
     * 修改保存活动
     */
    @RequiresPermissions("proxy:activity:edit")
    @Log(title = "活动", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestParam Map<String, Object> param) throws TException {
        param = MapUtil.filter(param, new Editor<Map.Entry<String, Object>>() {
            @Override
            public Map.Entry<String, Object> edit(Map.Entry<String, Object> entry) {
                if (entry.getKey().equals("invalidDate")) {
                    entry.setValue(DateUtil.parse(entry.getValue().toString(), DatePattern.NORM_DATETIME_PATTERN).getTime());
                }
                return entry;
            }
        });
        ActivityDataDTO activityDTO = BeanUtil.toBean(param, ActivityDataDTO.class);
        return toAjax(thriftUtil.getActivityDataService().save(activityDTO) > 0);
    }

    /**
     * 删除活动
     */
    @RequiresPermissions("proxy:activity:remove")
    @Log(title = "活动", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) throws TException {
        List<Integer> _ids = ProxyUtil.splitToInt(ids, ",");
        return toAjax(thriftUtil.getActivityDataService().remove(_ids) > 0);
    }

    /**
     * 查询字典详细
     */
    @RequiresPermissions("proxy:activity:list")
    @GetMapping("/activityId/{id}")
    public String detail(@PathVariable("id") Integer id, ModelMap mmap) throws TException {
        mmap.put("activity", thriftUtil.getActivityDataService().queryById(id));
        mmap.put("activityList", thriftUtil.getActivityDataService().queryAll());
        return "proxy/activity/item/item";
    }


    /**
     * 上传活动
     */
    @RequiresPermissions("proxy:activity:remove")
    @Log(title = "活动", businessType = BusinessType.OTHER)
    @PostMapping("/upload")
    @ResponseBody
    public AjaxResult upload(String ids) throws TException {
        List<Integer> _ids = ProxyUtil.splitToInt(ids, ",");
        return toAjax(thriftUtil.getActivityDataService().upload(_ids) > 0);
    }
}
