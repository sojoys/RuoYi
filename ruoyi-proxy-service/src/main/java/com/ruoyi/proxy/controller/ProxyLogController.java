package com.ruoyi.proxy.controller;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.api.common.PageDTO;
import com.ruoyi.api.log.QueryConditionDTO;
import com.ruoyi.api.log.QueryResultDTO;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 日志模块Controller
 *
 * @author SandKing
 * @date 2020-11-26
 */
@Controller
@RequestMapping("/proxy/log")
public class ProxyLogController extends BaseController {
    private String prefix = "proxy/log";

    @Autowired
    private ThriftUtil thriftUtil;

    @RequiresPermissions("proxy:log:view")
    @GetMapping()
    public String log() {
        return prefix + "/log";
    }

    /**
     * 查询日志列表
     */
    @RequiresPermissions("proxy:log:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestParam Map<String, String> param) throws TException {

        QueryConditionDTO conditionDTO = new QueryConditionDTO(new HashMap<>());
        PageDTO pageDTO = new PageDTO();

        for (Map.Entry<String, String> entry : param.entrySet()) {
            if (StrUtil.isBlank(entry.getValue())) {
                continue;
            }
            String key = entry.getKey();
            switch (key) {
                case "pageSize":
                    pageDTO.setPageSize(Integer.valueOf(entry.getValue()));
                    break;
                case "pageNum":
                    pageDTO.setPageNum(Integer.valueOf(entry.getValue()));
                    break;
                case "orderByColumn":
                    pageDTO.setOrderByColumn(entry.getValue());
                    break;
                case "isAsc":
                    pageDTO.setIsAsc(entry.getValue());
                    break;
                default:
                    conditionDTO.getCondition().put(key, entry.getValue());
                    break;
            }
        }
        QueryResultDTO resultDTO = thriftUtil.getLogService().queryList(conditionDTO, pageDTO);
        return new TableDataInfo(resultDTO.logs, resultDTO.total);
    }

//
//    @RequiresPermissions("proxy:role:detail")
//    @GetMapping("/detail/{id}")
//    public String detail(@PathVariable("id") String id, ModelMap mmap) throws TException {
//        String json = thriftUtil.getRoleService().detailed(id);
//        mmap.put("role", JSONUtil.parseObj(json));
//        return prefix + "/detail";
//    }


//    /**
//     * 导出角色管理列表
//     */
//    @RequiresPermissions("proxy:role:export")
//    @Log(title = "角色管理", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ResponseBody
//    public AjaxResult export(TRole tRole)
//    {
//        List<TRole> list = tRoleService.selectTRoleList(tRole);
//        ExcelUtil<TRole> util = new ExcelUtil<TRole>(TRole.class);
//        return util.exportExcel(list, "role");
//    }
//
//    /**
//     * 新增角色管理
//     */
//    @GetMapping("/add")
//    public String add()
//    {
//        return prefix + "/add";
//    }
//
//    /**
//     * 新增保存角色管理
//     */
//    @RequiresPermissions("proxy:role:add")
//    @Log(title = "角色管理", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    @ResponseBody
//    public AjaxResult addSave(TRole tRole)
//    {
//        return toAjax(tRoleService.insertTRole(tRole));
//    }
//
//    /**
//     * 修改角色管理
//     */
//    @GetMapping("/edit/{id}")
//    public String edit(@PathVariable("id") Long id, ModelMap mmap)
//    {
//        TRole tRole = tRoleService.selectTRoleById(id);
//        mmap.put("tRole", tRole);
//        return prefix + "/edit";
//    }
//
//    /**
//     * 修改保存角色管理
//     */
//    @RequiresPermissions("proxy:role:edit")
//    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
//    @PostMapping("/edit")
//    @ResponseBody
//    public AjaxResult editSave(TRole tRole)
//    {
//        return toAjax(tRoleService.updateTRole(tRole));
//    }
//
//    /**
//     * 删除角色管理
//     */
//    @RequiresPermissions("proxy:role:remove")
//    @Log(title = "角色管理", businessType = BusinessType.DELETE)
//    @PostMapping( "/remove")
//    @ResponseBody
//    public AjaxResult remove(String ids)
//    {
//        return toAjax(tRoleService.deleteTRoleByIds(ids));
//    }
}