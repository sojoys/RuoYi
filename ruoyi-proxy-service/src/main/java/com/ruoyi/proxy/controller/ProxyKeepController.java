package com.ruoyi.proxy.controller;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.api.keep.QueryConditionDTO;
import com.ruoyi.api.keep.QueryResultDTO;
import com.ruoyi.api.common.PageDTO;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import com.ruoyi.proxy.utils.ProxyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 留存统计Controller
 * 
 * @author SandKing
 * @date 2021-03-04
 */
@Controller
@RequestMapping("/proxy/keep")
public class ProxyKeepController extends BaseController
{
    private String prefix = "proxy/keep";

    @Autowired
    private ThriftUtil thriftUtil;

    @RequiresPermissions("proxy:keep:view")
    @GetMapping()
    public String keep()
    {
        return prefix + "/keep";
    }

    /**
     * 查询留存统计列表
     */
    @RequiresPermissions("proxy:keep:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(@RequestParam Map<String, String> param) throws TException {
        QueryConditionDTO conditionDTO = ProxyUtil.toBean(param, QueryConditionDTO.class);
        PageDTO pageDTO = BeanUtil.toBean(param, PageDTO.class);
        QueryResultDTO resultDTO = thriftUtil.getKeepService().queryList(conditionDTO, pageDTO);
        return new TableDataInfo(resultDTO.roles, resultDTO.total);
    }
}