package com.ruoyi.proxy.controller;

import com.ruoyi.api.info.InfoDTO;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.proxy.config.thrift.ThriftUtil;
import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 封禁管理Controller
 * 
 * @author SandKing
 * @date 2020-11-26
 */
@Controller
@RequestMapping("/proxy/info")
public class ProxyInfoController extends BaseController
{
    private String prefix = "proxy/info";

    @Autowired
    private ThriftUtil thriftUtil;

    // 系统介绍
    @GetMapping("/main")
    public String main(ModelMap mmap) throws TException {
        InfoDTO info = thriftUtil.getInfoService().getInfo();
        mmap.put("info", info);
        return prefix + "/main";
    }
}