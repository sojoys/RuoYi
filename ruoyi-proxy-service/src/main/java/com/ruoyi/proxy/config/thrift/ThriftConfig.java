package com.ruoyi.proxy.config.thrift;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ThriftConfig {
    @Value("${thrift.host}")
    private String host;

    @Value("${thrift.port}")
    private int port;

    @Value("${thrift.timeout}")
    private int timeout;

    @Bean
    public ThriftPool thriftPool() {
//        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
//        config.setJmxEnabled(false);
//
//
//        ThriftPool thriftPool = new ThriftPool(new ThriftPoolFactory(host, port, timeout),config);
//        return thriftPool;


        // 初始化对象池配置
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setJmxEnabled(false);
        poolConfig.setBlockWhenExhausted(true);
        poolConfig.setMaxWaitMillis(3000);
        poolConfig.setMinIdle(10);
        poolConfig.setMaxIdle(10);
        poolConfig.setMaxTotal(24);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestOnCreate(true);
        poolConfig.setTestWhileIdle(false);
        poolConfig.setLifo(false);
        // 初始化对象池
        ThriftPool thriftPool  = new ThriftPool(new TSocketFactory(host, port), poolConfig);
        return thriftPool;
    }


}