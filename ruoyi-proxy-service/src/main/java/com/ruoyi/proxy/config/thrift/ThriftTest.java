package com.ruoyi.proxy.config.thrift;

import com.ruoyi.api.common.PageDTO;
import com.ruoyi.api.notice.NoticeService;
import com.ruoyi.api.notice.QueryConditionDTO;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.thrift.TException;

public class ThriftTest {
    public static void main(String[] args) throws TException {

        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setJmxEnabled(false);


        ThriftPool thriftPool = new ThriftPool(new ThriftPoolFactory("127.0.0.1", 8632, 5000), config);

        ThriftProxy thriftProxy = new ThriftProxy();
        thriftProxy.setThriftPool(thriftPool);

        NoticeService.Iface noticeService = thriftProxy.getService(NoticeService.Iface.class);
        noticeService.queryList(new QueryConditionDTO(),new PageDTO());
    }
}
