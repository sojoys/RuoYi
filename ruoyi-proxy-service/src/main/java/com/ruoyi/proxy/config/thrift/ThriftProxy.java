package com.ruoyi.proxy.config.thrift;

import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TMultiplexedProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@Component
public class ThriftProxy {

    @Autowired
    private ThriftPool thriftPool;

    public void setThriftPool(ThriftPool thriftPool) {
        this.thriftPool = thriftPool;
    }

    public <T> T getService(final Class<?> interfaceClass) {
        // 创建动态代理对象
        return (T) Proxy.newProxyInstance(
                interfaceClass.getClassLoader(),
                new Class<?>[]{interfaceClass},
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        TSocket tSocket = null;
                        try {
                            tSocket = thriftPool.borrowObject();
                            TFramedTransport transport = new TFramedTransport(tSocket);
                            String serviceName = StrUtil.removeSuffix(interfaceClass.getName(), "$Iface");
                            TMultiplexedProtocol protocol = new TMultiplexedProtocol(new TCompactProtocol(transport), serviceName);

                            Class<?> clientClass = ClassUtil.loadClass(serviceName + "$Client",true);

                            Object client = ReflectUtil.newInstance(clientClass, protocol);

                            return ReflectUtil.invoke(client,method,args);
                        } catch (Throwable ex) {
                            if (tSocket != null) {
                                tSocket.close();
                            }
                            throw ex;
                        } finally {
                            thriftPool.returnObject(tSocket);
                        }
                    }
                }
        );
    }
}
