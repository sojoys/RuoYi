package com.ruoyi.proxy.config.thrift;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.thrift.transport.TSocket;

import java.net.Socket;

/**
 * TSocket池对象工厂，只使用了部分需要的方法
 * 
 * @author gqliu 2015年10月15日
 *
 */
public class TSocketFactory extends BasePooledObjectFactory<TSocket> {

    private String ip;
    private int port;

    public TSocketFactory(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    /**
     * 创建对象
     */
    @Override
    public TSocket create() throws Exception {
        TSocket socket = new TSocket(ip, port);
        socket.open();
        return socket;
    }

    /**
     * 用PooledObject封装对象放入池中
     */
    @Override
    public PooledObject<TSocket> wrap(TSocket socket) {
        return new DefaultPooledObject<TSocket>(socket);
    }

    /**
     * 销毁对象
     */
    @Override
    public void destroyObject(PooledObject<TSocket> p) throws Exception {
        TSocket socket = p.getObject();
        socket.close();
        super.destroyObject(p);
    }

    /**
     * 验证对象
     */
    @Override
    public boolean validateObject(PooledObject<TSocket> p) {
        TSocket t = p.getObject();
        Socket s = t.getSocket();
        boolean closed = s.isClosed();
        boolean connected = s.isConnected();
        boolean outputShutdown = s.isOutputShutdown();
        boolean inputShutdown = s.isInputShutdown();
        
        boolean urgentFlag = false;
        try {
            s.sendUrgentData(0xFF);
            urgentFlag = true;
        } catch (Exception e) {
            
        }
        
        return urgentFlag && connected && !closed && !inputShutdown && !outputShutdown;
    }
}