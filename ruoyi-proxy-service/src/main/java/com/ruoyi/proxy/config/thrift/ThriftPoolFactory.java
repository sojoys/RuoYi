package com.ruoyi.proxy.config.thrift;

import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.thrift.transport.TSocket;

public class ThriftPoolFactory implements PooledObjectFactory<TSocket> {
    private String host;
    private int port;
    private int timeout;

    public ThriftPoolFactory(String host, int port, int timeout) {
        this.host = host;
        this.port = port;
        this.timeout = timeout;
    }

    @Override
    public PooledObject<TSocket> makeObject() throws Exception {
        TSocket tSocket = new TSocket(host, port, timeout);
        tSocket.open();
        return new DefaultPooledObject<>(tSocket);
    }

    @Override
    public void destroyObject(PooledObject<TSocket> p) throws Exception {
        if (p != null && p.getObject() != null) {
            p.getObject().close();
        }
        p.markAbandoned();
    }

    @Override
    public boolean validateObject(PooledObject<TSocket> p) {
        return p != null && p.getObject() != null && p.getObject().isOpen();
    }

    @Override
    public void activateObject(PooledObject<TSocket> pooledObject) throws Exception {

    }

    @Override
    public void passivateObject(PooledObject<TSocket> pooledObject) throws Exception {

    }
}
