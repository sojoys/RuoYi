package com.ruoyi.proxy.config.thrift;

import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.AbandonedConfig;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.thrift.transport.TSocket;

public class ThriftPool extends GenericObjectPool<TSocket> {

    public ThriftPool(PooledObjectFactory<TSocket> factory) {
        super(factory);
    }

    public ThriftPool(PooledObjectFactory<TSocket> factory, GenericObjectPoolConfig<TSocket> config) {
        super(factory, config);
    }

    public ThriftPool(PooledObjectFactory<TSocket> factory, GenericObjectPoolConfig<TSocket> config, AbandonedConfig abandonedConfig) {
        super(factory, config, abandonedConfig);
    }
}
