package com.ruoyi.proxy.config.thrift;

import com.ruoyi.api.activity.ActivityDataService;
import com.ruoyi.api.activity.ActivityItemService;
import com.ruoyi.api.ban.BanService;
import com.ruoyi.api.circular.CircularService;
import com.ruoyi.api.config.ConfigService;
import com.ruoyi.api.dict.DictService;
import com.ruoyi.api.info.InfoService;
import com.ruoyi.api.keep.KeepService;
import com.ruoyi.api.log.LogService;
import com.ruoyi.api.mail.MailService;
import com.ruoyi.api.notice.NoticeService;
import com.ruoyi.api.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ThriftUtil {

    @Autowired
    private ThriftProxy thriftProxy;

    public ConfigService.Iface getConfigService() {
        ConfigService.Iface configService = thriftProxy.getService(ConfigService.Iface.class);
        return configService;
    }

    public NoticeService.Iface getNoticeService() {
        NoticeService.Iface noticeService = thriftProxy.getService(NoticeService.Iface.class);
        return noticeService;
    }

    public DictService.Iface getDictService() {
        DictService.Iface dictService = thriftProxy.getService(DictService.Iface.class);
        return dictService;
    }

    public MailService.Iface getMailService() {
        MailService.Iface mailService = thriftProxy.getService(MailService.Iface.class);
        return mailService;
    }

    public RoleService.Iface getRoleService() {
        RoleService.Iface roleService = thriftProxy.getService(RoleService.Iface.class);
        return roleService;
    }

    public BanService.Iface getBanService() {
        BanService.Iface banService = thriftProxy.getService(BanService.Iface.class);
        return banService;
    }

    public CircularService.Iface getCircularService() {
        CircularService.Iface circularService = thriftProxy.getService(CircularService.Iface.class);
        return circularService;
    }

    public InfoService.Iface getInfoService() {
        InfoService.Iface infoService = thriftProxy.getService(InfoService.Iface.class);
        return infoService;
    }

    public KeepService.Iface getKeepService() {
        KeepService.Iface keepService = thriftProxy.getService(KeepService.Iface.class);
        return keepService;
    }

    public LogService.Iface getLogService() {
        LogService.Iface logService = thriftProxy.getService(LogService.Iface.class);
        return logService;
    }

    public ActivityDataService.Iface getActivityDataService() {
        ActivityDataService.Iface activityDataService = thriftProxy.getService(ActivityDataService.Iface.class);
        return activityDataService;
    }

    public ActivityItemService.Iface getActivityItemService() {
        ActivityItemService.Iface activityItemService = thriftProxy.getService(ActivityItemService.Iface.class);
        return activityItemService;
    }
}
